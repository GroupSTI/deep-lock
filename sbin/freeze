#!/bin/sh

#### ------------------------------------------------------------------------------#
# 1) IDENTIFICAÇÃO/INFORMAÇÃO

# Script
NAME=freeze
DESC="Congela o sistema contra modificações, para um ou mais usuários"
VERSION="0.0.1"
DATE="11-02-2020"

#Author
AUTHOR="Rener Dias"
EMAIL="<renerdias@msn.com>"

#------------------------------------------------------------------------------ ####



#### ------------------------------------------------------------------------------#
# 2) VARIÁVEIS DE FORMATAÇÃO

# Para texto
TextRed='\033[0;31m'       # Red
TextGreen='\033[0;32m'     # Green
TextYellow='\033[0;33m'    # Yellow
TextPurple='\033[0;35m'    # Purple
TextCyan='\033[0;36m'      # Cyan
TextBlue='\033[0;44m'      # Blue
TextLightBlue='\033[0;94m' # LightBlue

# Para fundo(background)
BgRed='\033[0;41m'        # Red
BgGreen='\033[0;42m'      # Green
BgYellow='\033[0;43m'     # Yellow
BgPurple='\033[0;45m'     # Purple
BgCyan='\033[0;46m'       # Cyan
BgBlue='\033[0;44m'       # Blue
BgLightBlue='\033[0;104m' # LightBlue

# Redefinir cores
Color_Off='\033[0m' # Redefinir

# Exemplo de uso: echo "$TextCyan Adicionando repositórios... $Color_Off"

#------------------------------------------------------------------------------ ####




#### ------------------------------------------------------------------------------#
# 3) VARIÁVEIS DE EXECUÇÃO

### ATENÇÃO É NECESSÁRIO E URGENTE ENCONTRAR OUTRO LOCAL PARA ARMAZENAR O ARQUIVO DE SENHA
# Arquivo onde será salvo a senha de acesso ao script, caso seja informada
PASSWORD_FILE=.freeze-password

### ATENÇÃO É NECESSÁRIO E URGENTE:
# Trocar diretório para /home/.freeze e
# excluir ele no rsync com --exclude '$DIR_FREEZE'
# Diretório onde será salvo o backup dos arquivos do(s) usuário(s)
DIR_FREEZE=/etc/.freeze

# Arquivo que será executado na inicialização do sistema,
# para remoção das alterações feitas pelo usuário que teve sua sessão congelada
BOOT_FILE_EXEC=/etc/init.d/freeze

#------------------------------------------------------------------------------ ####




#### ------------------------------------------------------------------------------#
# 4) VERIFICAÇÃO DE PRIVILÉGIOS DE EXECUÇÃO

# Verifica se o script está sendo executado como root
if [ "$(id -u)" != "0" ]; then
	echo
	echo "$TextBlue Você deve executar este script como root! $Color_Off"
	echo
	exit 1
fi

#------------------------------------------------------------------------------ ####




#### ------------------------------------------------------------------------------#
# 5) FUNÇOES SECUNDÁRIAS

#===| 
security() {
	# Verifica se foi criado/existe um arquivo de senha
	if [ -e $PASSWORD_FILE ]; then
		# Pega o conteudo do arquivo
		#freeze_password=$(<$PASSWORD_FILE)
		# Pega a 1ª linha do arquivo
		freeze_password=$(head -1 $PASSWORD_FILE)
		echo "$BgYellow ATENÇÃO!!! $Color_Off Foi definida anteriormente uma senha para utilização do 'freeze'."
		echo
		read -p "Digite a senha: " password
		# Pega a senha digitda, faz o hash, e recorta de 1-32
		hash_password=$(echo -n "$password" | md5sum | cut -c 1-32)

		# Confere a senha digitada, com a senha previamente definida
		if [ $hash_password != $freeze_password ]; then
			echo
			echo
			echo "✘ Senha inválida!"
			echo
			exit 1
		fi
		echo
		echo
	fi
}

#===|
# Congelar o sistema para um usuário específico
start_user() {
	echo

	# Recebe o nome do usuário informado
	system_user=$1
	
	# Se existir o arquivo que inicia junto ao boot
	if [ -e $BOOT_FILE_EXEC ]; then
		grep -v "exit 0" $BOOT_FILE_EXEC > file_temp
		echo "rsync -a --delete $DIR_FREEZE/$system_user/ /home/$system_user/" >> file_temp
		echo "exit 0" >> file_temp
		rm -rf $BOOT_FILE_EXEC
		mv file_temp $BOOT_FILE_EXEC
		chmod +x $BOOT_FILE_EXEC
	else
		# Caso não exista, ele é criado
		create_boot_file "rsync -a --delete $DIR_FREEZE/$system_user/ /home/$system_user/"	
		if [ "$?" -ne "0" ]; then
			echo "$TextRed ✘ Desculpe, ocorreu um erro inesperado ao tentar congelar o usuário '$system_user'. Por favor, tente novamente...$Color_Off"
			return 1
		fi
	fi
		
	# Realiza um cópia dos arquivos/configurações do usuário
	rsync -a /home/$system_user $DIR_FREEZE/
	# Verifica se o rsync foi executado com sucesso
	if [ $? -eq 0 ]; then
		#echo "$TextGreen ✔ Usuário '$system_user' congelado com sucesso! $Color_Off"
		return 0
	fi
}

#===|
check_user() {

  # Recebe o nome do usuário
	system_user=$1
    
  if id -u "$system_user" > /dev/null 2>&1; then
	# Não da return, pq ainda tem que verificar se a pasta de usuário existe
    echo #return 0
  else
    return 1
  fi

	# Verificar se a pasta do usuario informado existe
	if [ ! -d /home/$system_user ]; then		
		return 1
	fi
	return 0
}

#===|
check_status() {

  	# Recebe o nome do usuário
	system_user=$1

	# Verificar se a pasta do usuario informado existe no diretorio de congelamento
	if [ ! -d $DIR_FREEZE/$system_user ]; then
		#echo "$TextRed ✘ Pasta de backup do usuário $system_user não existe! $Color_Off"
		return 1
	fi

	# Verificar se o arquivo de inicialização existe
	if [ ! -e $BOOT_FILE_EXEC ]; then
		echo "$TextRed ✘ Arquivo de inicialização não existe! $Color_Off"
		return 1
	fi

	# Verificar se no arquivo freeze existe o comando de limpeza de alterações para o usuario informado
	word="rsync -a --delete $DIR_FREEZE/$system_user"
	if grep "$word" $BOOT_FILE_EXEC > /dev/null
	then
		return 0
	else
		echo "$TextRed ✘ Não foi encontrado no arquivo de inicialização, referência ao usuário $system_user! $Color_Off"
		return 1
	fi
}

#===|
stop_user() {

  	# Recebe o nome do usuário
	system_user=$1

	# Se existir a pasta de congelamento do usuário
	if [ -d $DIR_FREEZE/$system_user ]; then
		# Retorna pasta do usuário ao estado original
		rsync -a --delete $DIR_FREEZE/$system_user/ /home/$system_user/
		if [ "$?" -eq "0" ]; then
			# Remove a pasta de backup do usuário
			rm -rf $DIR_FREEZE/$system_user
		else
			echo "$TextRed ✘ Não foi possível retornar a pasta do usuário $system_user ao seu estado original! $Color_Off"
			echo "$TextRed ✘ Descongelamento cancelado! $Color_Off"
			return 1
		fi
	fi

	# Se existir o arquivo que inicia junto ao boot
	if [ -e $BOOT_FILE_EXEC ]; then
		grep -v "/home/$system_user/" $BOOT_FILE_EXEC > file_temp
		rm -rf $BOOT_FILE_EXEC
		mv file_temp $BOOT_FILE_EXEC
		chmod +x $BOOT_FILE_EXEC
	fi

	return 0
}

#===|
cleaner() {
	# Se existir a pasta de congelamento do usuário
	if [ -d $DIR_FREEZE ]; then
		# Remove a pasta de backup
		rm -rf $DIR_FREEZE
		# echo "$TextYellow ✔ Pasta de backup removida! $Color_Off"
	fi

	# Se existir o arquivo que inicia junto ao boot, remove
	if [ -e $BOOT_FILE_EXEC ]; then
    	# Remove todos os link's conforme informado no cabeçalho (Default-Start e Default-Stop)
		update-rc.d freeze remove
		rm -rf $BOOT_FILE_EXEC
		# echo "$TextYellow ✔ Arquivos de inicialização removidos! $Color_Off"
	fi

	# Se existir o arquivo de senha, remove
	if [ -e $PASSWORD_FILE ]; then
		rm -rf $PASSWORD_FILE
		# echo "$TextYellow ✔ Senha removida! $Color_Off"
	fi
}

#===|
set_password() {

	new_password="$1"
	password_length=${#new_password}
	
	if [ $password_length = 0 ]; then
		if [ -e $PASSWORD_FILE ]; then
			rm -rf $PASSWORD_FILE
		fi
		echo
		echo
		echo "✔ Senha removida com sucesso."
		echo
	else
		hash_password=$(echo -n "$new_password" | md5sum | cut -c 1-32)
		echo -n "$hash_password" >$PASSWORD_FILE
		echo
		echo
		echo "✔ Senha alterada com sucesso."
		echo
	fi
}


#===|
user_list_freeze() {
echo
		echo "$BgGreen Lista de usuários congelados: $Color_Off"
		echo $TextGreen
		for file in ${DIR_FREEZE}/*; do
    	if [ -d "$file" ]; then
				echo "   ⬤ $(basename $file)"
    	fi
		done
		echo $Color_Off
}

#===|
usage() {
    echo
    echo "usage: $NAME [options] [arguments]"
    echo
    echo "options:"
    echo "     start"
    echo "          arguments:"
    echo "               all           - Congela todos os usuários do sistema"
    echo "               this          - Congela o usuário atual"
    echo "               *             - Nome do usuário específico a ser congelado"
    echo "     stop"
    echo "          ✘ arguments:"
    echo "               all           - Todos os usuários do sistema"
    echo "               this          - Usuário atual"
    echo "               *             - Nome do usuário específico"
    echo "     status"
    echo "          ✘ arguments:"
    echo "               all           - Todos os usuários do sistema"
    echo "               this          - Usuário atual"
    echo "               *             - Nome do usuário específico"
    echo "     list                    - Lista de usuários congelados"
    echo "     password                - Define um senha para o congelamento"
    echo "     usage                   - Exibe instruções de uso"
    echo
}


#------------------------------------------------------------------------------ ####




#### ------------------------------------------------------------------------------#
# 6) FUNÇOES PRIMÁRIAS

#===|
status_freeze() {

	echo
	# Verificar quantidade de parametros
	if [ $# -lt 1 ]; then
   		echo "$BgRed ERROR $Color_Off Este comando precisa do nome do usuário como parâmetro."
		echo
		usage
		exit 1
	fi

	# Verifica se a variável é nula
	if [ -z $1 ]; then
		echo "$BgRed ERROR $Color_Off Este comando precisa do nome do usuário como parâmetro."
		echo
		usage
		exit 1
	fi

  	# Recebe o nome do usuário
	system_user=$1

	check_status $system_user
	if [ "$?" -eq "0" ]; then
		echo "${TextYellow} ✔ O usuário ${Color_Off}${BgYellow} ${system_user} ${Color_Off}${TextYellow} está congelado!${Color_Off}"
	else
		echo "${TextYellow} ✘ O usuário ${Color_Off}${BgYellow} ${system_user} ${Color_Off}${TextYellow} não está congelado!${Color_Off}"
	fi
    echo
	exit 0	
}

#===|
stop_freeze_user() {

	echo
	# Verificar quantidade de parametros
	if [ $# -lt 1 ]; then
   		echo "$BgRed ERROR $Color_Off Este comando precisa do nome do usuário como parâmetro."
		echo
		usage
		exit 1
	fi

	# Verifica se a variável é nula
	if [ -z $1 ]; then
		echo "$BgRed ERROR $Color_Off Este comando precisa do nome do usuário como parâmetro."
		echo
		usage
		exit 1
	fi

  	# Recebe o nome do usuário
	system_user=$1

	# Verifica se usuário existe
	check_user $system_user
	if [ "$?" -ne "0" ]; then
		echo "$TextRed ✘ '$system_user' não é um usuário válido! $Color_Off"
		exit 1
	fi

	# Verifica o status do sistema
	check_status $system_user
	if [ "$?" -ne "0" ]; then
		echo "$TextLightBlue ✘ Não foi possivel realizar esta operação, o usuário '$system_user' não está congelado.$Color_Off"
		exit 0
	fi	
	
	echo
	echo "$TextLightBlue Descongelando o sistema para ${system_user}. Por favor aguarde...$Color_Off"
	echo
	
	stop_user $system_user
	if [ "$?" -eq "0" ]; then
		echo "$TextGreen ✔ Usuário '${system_user}' descongelado com sucesso! $Color_Off"
	else
		echo "$TextRed ✘ Não foi possível descongelar o sistema para ${system_user}! $Color_Off"
		exit 1
	fi
	echo
	exit 0
}

#===|
# Congelar o sistema para o usuario ativo/logado apenas
stop_freeze_this() {
	# Há 3 formas de se pegar o nome do usuário que está executando o arquivo
	#		1) usuario=$(whoami)
	#		2) usuario="$USER"
	#		3) usuario=id -un
	
	# Há 2 formas de se pegar o nome do usuário que está logado/ativo
	#		1) usuario=$(who | awk '{print $1}')
	#		5) who | awk '{print $1}' # Este só imprimi, não retorna para variável	

	system_user=$(who | awk '{print $1}')

	# Congela o usuário específico
	stop_freeze_user $system_user
}

#===|
# Congelar o sistema para o usuario ativo/logado apenas
start_freeze_this() {
	# Há 3 formas de se pegar o nome do usuário que está executando o arquivo
	#		1) usuario=$(whoami)
	#		2) usuario="$USER"
	#		3) usuario=id -un
	
	# Há 2 formas de se pegar o nome do usuário que está logado/ativo
	#		1) usuario=$(who | awk '{print $1}')
	#		5) who | awk '{print $1}' # Este só imprimi, não retorna para variável	

	system_user=$(who | awk '{print $1}')

	# Congela o usuário específico
	start_freeze_user $system_user
}

#===|
# Congelar o sistema para um usuário específico
start_freeze_user() {
	echo
	# Verificar quantidade de parametros
	if [ $# -lt 1 ]; then
   		echo "$BgRed ERROR $Color_Off Este comando precisa do nome do usuário como parâmetro."
		echo
		usage
		exit 1
	fi

	# Verifica se a variável é nula
	if [ -z $1 ]; then
		echo "$BgRed ERROR $Color_Off Este comando precisa do nome do usuário como parâmetro."
		echo
		usage
		exit 1
	fi

	# Recebe o nome do usuario informado
	system_user=$1
	
	# Verifica se usuário existe
	check_user $system_user
	if [ "$?" -ne "0" ]; then
		echo "$TextRed ✘ '$system_user' não é um usuário válido! $Color_Off"
		echo
		exit 1
	fi

	# Verifica o status do sistema
	check_status $system_user
	if [ "$?" -eq "0" ]; then
		echo "$TextLightBlue ✘ Operação abortada, o usuário '$system_user' já está congelado.$Color_Off"
		echo
		exit 0
	fi

	start_user $system_user
	if [ "$?" -eq "0" ]; then
		echo "$TextGreen ✔ Usuário '$system_user' foi congelado com sucesso! $Color_Off"
		echo
		exit 0
	else
		echo "$TextRed ✘ Não foi possível congelar o sistema para ${system_user}! $Color_Off"
		echo
		exit 1
	fi
}

#===|
# Congelar o sistema para todos os usuários
start_freeze_all() {
	echo
	#MYVAR=ho02123ware38384you443d34o3434ingtod38384day
	#echo "$MYVAR" | sed -e 's/[a-zA-Z]/X/g' -e 's/[0-9]/N/g'

	for dir in $(ls /home); do
		# Recebe o nome do usuario informado
		system_user=$dir
		
		# Verifica se usuário existe
		check_user $system_user
		if [ "$?" -ne "0" ]; then
			#echo "$TextRed ✘ '$system_user' não é um usuário válido! $Color_Off"
			continue
		fi

		# Verifica o status do sistema
		check_status $system_user
		if [ "$?" -ne "0" ]; then
			start_user $system_user
			if [ "$?" -eq "0" ]; then
				echo "$TextGreen ✔ Usuário '$system_user' foi congelado com sucesso! $Color_Off"
			else
				echo "$TextRed ✘ Não foi possível congelar o sistema para ${system_user}! $Color_Off"
			fi
		else
			echo "$TextLightBlue ✘ Operação abortada, o usuário '$system_user' já está congelado.$Color_Off"
		fi
		echo
	done
}

#===|
# Descongelar o sistema para todos os usuários
stop_freeze_all() {
	echo

	for dir in $(ls /home); do
		# Recebe o nome do usuario informado
		system_user=$dir
		
		# Verifica se usuário existe
		check_user $system_user
		if [ "$?" -ne "0" ]; then
			#echo "$TextRed ✘ '$system_user' não é um usuário válido! $Color_Off"
			continue
		fi

		# Verifica o status do sistema
		check_status $system_user
		if [ "$?" -eq "0" ]; then
			stop_user $system_user
			if [ "$?" -eq "0" ]; then
				echo "$TextGreen ✔ Usuário '$system_user' descongelado com sucesso! $Color_Off"
			else
				echo "$TextRed ✘ Não foi possível descongelar o sistema para ${system_user}! $Color_Off"
			fi
		else
			echo "$TextLightBlue ✘ Operação abortada, o usuário '$system_user' não está congelado.$Color_Off"
		fi
		echo
	done
}

#===|
# Cria o arquivo que realizará a limpeza das alterações do(s) usuários, na inicialização do sistema
# Recebe como parâmetro, o comando a ser executado na inicialização
create_boot_file() {
	
	# Remove possíveis sujeiras
	cleaner
	
    echo "#!/bin/sh" >freeze_temp
    echo "#" >>freeze_temp
    echo "### BEGIN INIT INFO" >>freeze_temp
    echo "# Provides:          freeze" >>freeze_temp
    #echo "# Required-Start:    remote_fs syslog time" >>freeze_temp
    #echo "# Required-Stop:     remote_fs syslog time" >>freeze_temp
    #echo "# Should-Start:      network named slapd autofs ypbind nscd nslcd winbind" >>freeze_temp
    #echo "# Should-Stop:       network named slapd autofs ypbind nscd nslcd winbind" >>freeze_temp
    echo "# Default-Start:     2 3 4 5" >>freeze_temp
    echo "# Default-Stop:	   0 1 6" >>freeze_temp
    echo "# Short-Description: Remove modificações realizadas pelo usuário após o congelamento" >>freeze_temp
    echo "# Description:       Executa a limpeza das alterações feitas pelo(s) usuário(s) " >>freeze_temp
    echo "#                    durante o congelamento do sistema " >>freeze_temp
    echo "### END INIT INFO" >>freeze_temp
    echo "" >>freeze_temp
    echo "NAME=freeze" >>freeze_temp
    echo "" >>freeze_temp
    echo "$1" >>freeze_temp
    echo "" >>freeze_temp
    echo "exit 0" >>freeze_temp
    # //TODO Verificar a necessidade de criar um arquivo temporário, ao invés de criar o arquivo direto
    chmod +x freeze_temp
    cp -p freeze_temp $BOOT_FILE_EXEC
    rm -rf freeze_temp
	# Adicionar link's nos vários niveis de inicialização do sistema
	# Lembrando que update-rc.d, usa as informações do cabeçalho Default-Start e Default-Stop
	update-rc.d $NAME defaults

	# Cria o diretório de congelamento
	mkdir $DIR_FREEZE/

	# Forçar a permissão de execução no arquivo de freeze
	chmod +x $BOOT_FILE_EXEC
	
	return 0
}
#------------------------------------------------------------------------------ ####




#### ------------------------------------------------------------------------------#
# 6) INICIALIZAÇÃO

# Inicializa fazendo checagem de segurança
security

case "$1" in
start)
	case "$2" in
	all) start_freeze_all ;;
	this) start_freeze_this;;
	*) start_freeze_user $2 ;;
	esac
	;;
stop)
	case "$2" in
	all) stop_freeze_all ;;
	this) stop_freeze_this;;
	*) stop_freeze_user $2 ;;
	esac
	;;
status)
	status_freeze $2
	;;
list)
	user_list_freeze
	;;
password)
	set_password "$2"
	;;
*) usage
	exit 3
	;;
esac
#------------------------------------------------------------------------------ ####

# //TODO: Proteção apenas das configurações do sistema/aplicativos (arquivos ocultos)
# //TODO: Não congela as pastas visíveis, permitindo persistir arquivo nelas
# //TODO: Precisa rever os parametros, a maioria não é necessária
# rsync -rtxvzlbyh --include=".*" --exclude="*[!.]" ./pasta_usuario ./pasta_usuario_congelamento
# rsync -rtxvzlbyh --delete --include=".*" --exclude="*[!.]" ./pasta_usuario_congelamento ./pasta_usuario
